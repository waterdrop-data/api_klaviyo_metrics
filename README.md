
## What does the function do?

The functin collects all placed orders since the last run and insert them to a bigquery table

### How do I get set up? ###

`pip install -r /path/to/requirements.txt`

### Start the local cloud function testing framework for the function
 - set the IS_LOCAL variable in main.py to a non None value, for local testing
 - `functions-framework --target=klaviyo_update_placed_orders`

### How should the Request look like?
- The request should always conatin the klaviyo store from which the placed orders should be updated
* Local
 - `curl http://localhost:8080 -H "Content-Type:application/json"  -d '{"store":"fr"}'`
* Cloud
    Invoke function with:
    - `curl -X POST HTTP_TRIGGER_ENDPOINT -H "Content-Type:application/json"  -d '{"store":"fr"}'`
    - HTTP_TRIGGER_ENDPOINT: https://europe-west3-waterdrop-dataplatform.cloudfunctions.net/cf-klaviyo-api-update-placed-orders
