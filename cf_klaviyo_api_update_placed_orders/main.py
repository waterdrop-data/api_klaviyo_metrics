from flask import escape

from pandas.io import gbq
import requests
import json
import pandas as pd
import pandas_gbq as pdg
import numpy as np
import os
import tqdm
from  google.cloud import bigquery
from google.oauth2 import service_account
from google.auth import default
import yaml
# Import the Secret Manager client library.
from google.cloud import secretmanager

def credentials():
    # To use this file locally set $IS_LOCAL=1 and populate environment variable $GOOGLE_APPLICATION_CREDENTIALS with path to keyfile
    # Get Application Default Credentials if running in Cloud Functions
    if os.getenv("IS_LOCAL") is None:
        credentials, project = default(scopes=["https://www.googleapis.com/auth/cloud-platform"])
    else:
        #Get project root path and cedentials
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) # This is your Project Root
        #Service account as env variable
        sa = os.path.join(ROOT_DIR, 'waterdrop-dataplatform-68bd89470d08.json')
        credentials = service_account.Credentials.from_service_account_file(sa, scopes=["https://www.googleapis.com/auth/cloud-platform"],
        )
    return credentials

def access_secret_version(project_id, version_id, store, service_account):
    secret_id = f'wd_klaviyo_{store}'
    """
    Access the payload for the given secret version if one exists. The version
    can be a version number as a string (e.g. "5") or an alias (e.g. "latest").
    """
    # Create the Secret Manager client.
    client = secretmanager.SecretManagerServiceClient(credentials=credentials())
    # Build the resource name of the secret version.
    name = f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"
    # Access the secret version.
    response = client.access_secret_version(request={"name": name})
    secret = response.payload.data.decode("UTF-8")
    return(secret)

def get_metric_id(api_key):
    column_list = ['data_object', 'data_id', 'data_name', 'data_created', 'data_updated', 'data_integration.object', 'data_integration.id', 'data_integration.name', 
                    'data_integration.category', 'object', 'page', 'start', 'end', 'total', 'page_size']
    df_result = pd.DataFrame(columns=column_list)
    url = f'https://a.klaviyo.com/api/v1/metrics'
    headers = {"Accept": "application/json"}
    counter = 0
    next = 0
    while next is not None:
        querystring = {f"api_key":{api_key},"page":{counter},"count":"500", "max":"500"}
        response = requests.request("GET", url, headers=headers, params=querystring)
        json_resp = json.loads(response.text)
        df = pd.json_normalize(json_resp,'data',['object','page', 'start', 'end', 'total', 'page_size'], record_prefix='data_', errors='ignore')
        df_result = df_result.append(df)
        if max(df.loc[:, 'page_size']) < 100:
            next = None
        counter += 1
    return list(df_result[df_result['data_name'] == 'Placed Order']['data_id'].array)

def query_next_bigquery(client,project, dataset, table):
    query_job = client.query(
        f"""
        SELECT max(timestamp) as last_import FROM `{project}.{dataset}.{table}` 
        """
    )
    results = query_job.result()  # Waits for job to complete.
    results_conv = []
    for res in results:
        results_conv.append(res.last_import)
    return results_conv[0]

def main(country):
    bq_project = 'waterdrop-dataplatform'
    bq_dataset  = 'wd_api_klaviyo'
    bq_table_metrics = f'{country}_placed_orders'
    secret_version_id = 1

    #Google settings
    client = bigquery.Client(credentials=credentials())

    #Klaviyo settings
    api_key = access_secret_version(bq_project, secret_version_id, country, service_account)
    metric_ids = get_metric_id(api_key) # => Metric for placed order

    total_orders = 0
    for metric_id in metric_ids:
        url = f'https://a.klaviyo.com/api/v1/metric/{metric_id}/timeline'
        headers = {"Accept": "application/json"}

        next = query_next_bigquery(client, bq_project,bq_dataset, bq_table_metrics )

        if next is None:
            next = 0
        while next is not None:
            counter = 0
            column_list = ['uuid','timestamp','datetime', 'event_properties.$extra.customer.email',
                'event_properties.$extra.customer.id','event_properties.$extra.order_number','event_properties.$extra.currency','event_properties.$value',
                'event_properties.$attribution.$message', 'event_properties.$attribution.$variation' , 'event_properties.$attribution.$flow', 'event_properties.Total Discounts']

            df_filtered_temp = pd.DataFrame(columns=column_list)
            while counter < 50:
                if next is None:
                    break
                #get results
                if next == 0:
                    querystring = {"api_key":api_key ,"count":"100","sort":"asc"}
                else:   
                    querystring = {"api_key":api_key ,"count":"100","sort":"asc", "since": next}
                response = requests.request("GET", url, headers=headers, params=querystring)
                json_resp = json.loads(response.text)

                #df = pd.json_normalize(json_resp,'data',['next'], errors='ignore')
                df_general_temp = pd.json_normalize(json_resp, errors='ignore')
                #print(df_general.head(10))

                #next_element
                next = df_general_temp['next'].unique()[0]
                count = df_general_temp['count'].unique()[0]
                #print(f'NEXT: {next}, COUNT: {count}')

                df_data = pd.json_normalize(json_resp['data'], errors='ignore')
                
                for col in column_list:
                    if col not in df_data.columns:
                        df_data[col] = np.nan

                temp_data = df_data.loc[:,column_list]
                #Filtered dataframe 
                df_filtered_temp = df_filtered_temp.append(temp_data, ignore_index=True)
                #print(f'Dataframe Length: {df_filtered_temp.shape[0]}')
                counter += 1
                total_orders += df_filtered_temp.shape[0]
            print(f'Total Orders updated in {country}-store: {total_orders}')
                
            df_filtered_temp.rename(columns={'event_properties.$extra.customer.email':'email',
                                                    'event_properties.$extra.customer.id':'customer_id',
                                                    'event_properties.$extra.order_number':'order_number',
                                                    'event_properties.$extra.currency':'currency',
                                                    'event_properties.$value':'value',
                                                    'event_properties.$attribution.$message':'campaign_id',
                                                    'event_properties.$attribution.$variation':'ab_variation_id',
                                                    'event_properties.$attribution.$flow':'flow_id',
                                                    'event_properties.Total Discounts':'discount_value'
                                                    }, 
                                        inplace=True)    
            schema = [
                    {'name':'uuid', 'type':'STRING'},
                    {'name':'timestamp', 'type':'INTEGER'},
                    {'name':'datetime', 'type':'TIMESTAMP'},
                    {'name':'email', 'type':'STRING'},
                    {'name':'customer_id', 'type':'INTEGER'},
                    {'name':'order_number', 'type':'INTEGER'},
                    {'name':'currency', 'type':'STRING'},
                    {'name':'value', 'type':'FLOAT'},
                    {'name':'campaign_id', 'type':'STRING'},
                    {'name':'ab_variation_id', 'type':'STRING'},
                    {'name':'flow_id', 'type':'STRING'},
                    {'name':'discount_value', 'type':'FLOAT'}
                    ]
            df_filtered_temp.to_gbq(f"{bq_dataset}.{bq_table_metrics}",project_id = bq_project, if_exists='append',table_schema=schema, progress_bar=True)
    return (total_orders)


def klaviyo_update_placed_orders(request):
    """HTTP Cloud Function.
    Args:
        request (flask.Request): The request object.
        <https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data>
    Returns:
        The response text, or any set of values that can be turned into a
        Response object using `make_response`
        <https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response>.
    """
    request_json = request.get_json(force=True,silent=True)
    request_args = request.args

    if request_json and 'store' in request_json:
        name = request_json['store']
    elif request_args and 'store' in request_args:
        name = request_args['store']
    else:
        name = 'dach'

    #commented => in cloud
    #anything else => local execution
    #os.environ["IS_LOCAL"] = None
    total_orders = main(name)
    return 'Updated orders from {}-store: {}'.format(name.upper(),escape(total_orders))
